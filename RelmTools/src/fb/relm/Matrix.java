package fb.relm;

public class Matrix {
    public static Matrix identity(int size) {
        double[][] mat = new double[size][size];
        for(int i = 0; i < size; ++i)
            mat[i][i] = 1;
        return new Matrix(mat);
    }
    
    public double[][] mat;
    
    public Matrix(double[][] mat) {
        this.mat = mat;
    }
    
    public Matrix(int rows, int cols) {
        this(new double[rows][cols]);
    }
    
    public Matrix(Matrix matrix) {
        this(matrix.mat.clone());
    }
    
    @Override
    protected Matrix clone() {
        return new Matrix(this);
    }
    
    public int rows() {
        return mat.length;
    }
    
    public int cols() {
        return mat[0].length;
    }
    
    public Matrix fill(double value) {
        for(int i = 0; i < rows(); ++i)
            for(int j = 0; j < cols(); ++j)
                mat[i][j] += value;
        return this;
    }
    
    public Matrix add(double value) {
        for(int i = 0; i < rows(); ++i)
            for(int j = 0; j < cols(); ++j)
                mat[i][j] += value;
        return this;
    }
    
    public Matrix mult(double value) {
        for(int i = 0; i < rows(); ++i)
            for(int j = 0; j < cols(); ++j)
                mat[i][j] *= value;
        return this;
    }
    
    public Matrix addMat(Matrix matrix) {
        Matrix ret = new Matrix(Math.max(rows(), matrix.rows()), Math.max(cols(), matrix.cols()));
        for(int i = 0; i < ret.rows(); ++i)
            for(int j = 0; j < ret.cols(); ++j) {
                ret.mat[i][j] = i < rows() && j < cols() ? mat[i][j] : 0d;
                if(i < matrix.rows() && j < matrix.cols())
                    ret.mat[i][j] += matrix.mat[i][j];
            }
        return ret;
    }
    
    public Matrix subMat(Matrix matrix) {
        Matrix ret = new Matrix(Math.max(rows(), matrix.rows()), Math.max(cols(), matrix.cols()));
        for(int i = 0; i < ret.rows(); ++i)
            for(int j = 0; j < ret.cols(); ++j) {
                ret.mat[i][j] = i < rows() && j < cols() ? mat[i][j] : 0d;
                if(i < matrix.rows() && j < matrix.cols())
                    ret.mat[i][j] -= matrix.mat[i][j];
            }
        return ret;
    }
    
    public Matrix multMat(Matrix matrix) throws Exception {
        if(cols() != matrix.rows())
            throw new Exception("Wrong sizes!");
        Matrix ret = new Matrix(rows(), matrix.cols());
        
        for(int i = 0; i < rows(); ++i)
            for(int j = 0; j < rows(); ++j) {
                ret.mat[i][j] = 0;
                for(int k = 0; k < cols(); ++k)
                    ret.mat[i][j] += mat[i][k]*matrix.mat[k][j];
            }
        
        return ret;
    }
    
    public double subDet(int i, int j) throws Exception {
        if(rows() != cols())
            throw new Exception("Not square matrix!");
        
        Matrix subMat = new Matrix(rows()-1, cols()-1);
        for(int ii = 0; ii < rows(); ++ii)
            if(ii != i)
                for(int jj = 0; jj < cols(); ++jj)
                    if(jj != j)
                        subMat.mat[ii < i ? ii : ii-1][jj < j ? jj : jj-1] = mat[ii][jj];
        return subMat.det();
    }
    
    public double det() throws Exception {
        if(rows() != cols())
            throw new Exception("Not square matrix!");
        
        if(rows() == 1)
            return mat[0][0];
        
        double ret = 0;
        
        for(int i = 0; i < rows(); ++i)
            ret += (i%2 == 0 ? 1 : -1)*subDet(i, 0);
        
        return ret;
    }
    
    public boolean equals(Matrix matrix, double error) {
        if(matrix.rows() != rows() || matrix.cols() != cols())
            return false;
        
        for(int i = 0; i < rows(); ++i)
            for(int j = 0; j < cols(); ++j)
                if(Math.abs(mat[i][j] - matrix.mat[i][j]) > error)
                    return false;
        
        return true;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj.getClass() != getClass())
            return false;
        
        return equals((Matrix)obj, 0);
    }
    
    public String toString(String sep) {
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        for(int i = 0; i < rows(); ++i) {
            if(i != 0)
                sb.append(sep);
            sb.append('{');
            sb.append(mat[i][0]);
            for(int j = 1; j < cols(); ++j)
                sb.append(sep).append(mat[i][j]);
            sb.append('}');
        }
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public String toString() {
        return toString(",");
    }
}
