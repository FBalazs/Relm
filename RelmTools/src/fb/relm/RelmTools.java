package fb.relm;

public class RelmTools {
    public static Matrix[] lagrange(Matrix A, double[] eigs) throws Exception {
        Matrix[] ret = new Matrix[eigs.length];
        
        for(int i = 0; i < ret.length; ++i) {
            ret[i] = Matrix.identity(eigs.length);
            for(int j = 0; j < ret.length; ++j)
                if(j != i) {
                    Matrix mat = A.subMat(Matrix.identity(ret.length).mult(eigs[j]));
                    mat.mult(1/(eigs[i]-eigs[j]));
                    ret[i] = ret[i].multMat(mat);
                }
        }
        
        return ret;
    }
    

    public static void printName(String name) {
        System.out.println();
        System.out.print(name);
        System.out.println(" =");
    }
    
    public static void printNum(double d, int dec) {
        System.out.format("%+."+dec+"f", d);
    }
    
    public static void printMat(String name, Matrix mat, int dec) {
        printName(name);
        for(int i = 0; i < mat.rows(); ++i) {
            for(int j = 0; j < mat.cols(); ++j)
                System.out.format("%+."+dec+"f ", mat.mat[i][j]);
            System.out.println();
        }
    }
    
    public static void main(String[] args) throws Exception {
        Matrix A = new Matrix(new double[][]{{-9.4785, 2.1262},
                                             {0.6644, -3.3215}});
        double[] eigs = new double[]{-9.69997, -3.10003};
        
        Matrix B = new Matrix(new double[][]{{-1.4},
                                             {-0.5}});
        Matrix C = new Matrix(new double[][]{{-1.6,-1.9}});
        double D = 1.8;
        
        printMat("A", A, 5);
        
        Matrix[] L = lagrange(A, eigs);
        Matrix sumI = new Matrix(L.length, L.length).fill(0);
        Matrix sumA = new Matrix(L.length, L.length).fill(0);
        for(int i = 0; i < L.length; ++i) {
            printMat("L"+(i+1), L[i], 5);
            sumI = sumI.addMat(L[i]);
            sumA = sumA.addMat(L[i].clone().mult(eigs[i]));
        }
        printMat("A", A, 5);
        printMat("sumA", sumA, 5);
        printMat("sumI", sumI, 5);
        
        System.out.println();
        System.out.println("A =    "+A);
        System.out.println("sumA = "+sumA);
        if(!A.equals(sumA, 0.000000001))
            System.err.println("!A.equals(sumA)");
        
        System.out.println(C.multMat(L[0]).multMat(B).rows()+" "+C.multMat(L[0]).multMat(B).cols());
        System.out.println(C.multMat(L[1]).multMat(B).rows()+" "+C.multMat(L[1]).multMat(B).cols());
        
        double a = D;
        double b = C.multMat(L[0]).multMat(B).mat[0][0];
        double c = eigs[0];
        double d = C.multMat(L[1]).multMat(B).mat[0][0];
        double f = eigs[1];
        
        System.out.println();
        System.out.format("a = %f\n", a);
        System.out.format("b = %f\n", b);
        System.out.format("c = %f\n", c);
        System.out.format("d = %f\n", d);
        System.out.format("f = %f\n", f);
        
//        Matrix mat1 = new Matrix(new double[][]{{1,0,2},{-1,3,1}});
//        Matrix mat2 = new Matrix(new double[][]{{3,1},{2,1},{1,0}});
//        printMat("mat1", mat1, 5);
//        printMat("mat2", mat2, 5);
//        printMat("mat1 x mat2", mat1.multMat(mat2), 5);
//        printMat("mat2 x mat1", mat2.multMat(mat1), 5);
    }
}
